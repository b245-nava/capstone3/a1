import {Fragment, useContext, useState, useEffect} from 'react';
import {Button, Table, Row, Col, Form, Modal} from 'react-bootstrap';
import UserContext from '../../UserContext.js';
import {Navigate, useNavigate, Link} from 'react-router-dom';
// import UpdateProd from './UpdateProd.js';
// import ArchiveItem from './ArchiveItem.js';
import Swal from 'sweetalert2';

export default function OrderItems ({prodProp}) {

	// Destructuring of product info
	const {_id, name, description, price, itemClass, image, isAvailable} = prodProp;

	// UseStates
	const [nameB, setNameB] = useState(name);
	const [descriptionB, setDescriptionB] = useState(description);
	const [priceB, setPriceB] = useState(price);
	const [typeB, setTypeB] = useState(itemClass);
	const [imageB, setImageB] = useState(image);

	const navigate = useNavigate();

	// UserContext
	const {user} = useContext(UserContext);

	// Model useStates
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	function editItem(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/edit/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: nameB,
				description: descriptionB,
				price: priceB,
				itemClass: typeB,
				image: imageB
			})
		})
		.then(result => result.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Product updated!',
					icon: 'success'
				})

				navigate("/admin/allProducts");
			} else {
				Swal.fire({
					title: 'Update failed.',
					text: 'See console for more info.',
					icon: 'error'
				})
			}
		})
		// .catch(err => console.log(err));
	};

	const archive = (event) => {

		event.preventDefault();

		fetch(`${process.env.REACT_APP_CAPSTONE2}/product/archive/${_id}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result=>result.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Item updated!',
					icon: 'success'
				})

				navigate('/admin/allProducts');

			} else {
				Swal.fire({
					title: 'Update failed.',
					text: 'See console for more info.',
					icon: 'error'
				})
			}
		})
		// .catch(err=>console.log(err))
	};


	return(
		<Fragment>
			<tbody>
				<tr>
					<td>{_id}</td>
					<td>{name}</td>
					<td>{description}</td>
					<td>{price}</td>
					<td>{itemClass}</td>
					<td>
						<Row>
						{/*Button for changing product availability*/}
						{
							isAvailable
							?
							<Button variant = "warning" className = "w-100" onClick={(event)=>archive(event)}>
								Remove
							</Button>
							:
							<Button variant = "info" className = "w-100" onClick={(event)=>archive(event)}>
								Add
							</Button>
						}
						</Row>
						<Row className="mt-1">
							<Button variant="success" onClick={(event) => setShow(true)}>
						        Update
						    </Button>
						    <Modal show={show} close={(event)=>setShow(false)}/>
						</Row>
					</td>
				</tr>
			</tbody>

			<Modal
		        show={show}
		        cancel={show.close}
		        size="lg"
		        aria-labelledby="contained-modal-title-vcenter"
		        centered
		        onSubmit = {(event) => editItem(event)}
			>
		        <Modal.Header>
		          <Modal.Title>Edit Product Details</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>
	        		<Row>
	        			<Col>
	        				<Form className = "mt-3" onSubmit = {event => editItem(event)}>
	        					<Row>
	        						<Col>
	        							<Form.Group className="mb-3" controlId="formBasicProdName">
	        							  <Form.Label>Product Name:</Form.Label>
	        							  <Form.Control 
	        							  type="string" 
	        							  placeholder="Enter product name" 
	        							  value={nameB}
	        							  onChange = {event => setNameB(event.target.value)}
	        							  required
	        							  />
	        							</Form.Group>
	        						</Col>
	        						<Col>
	        							<Form.Group className="mb-3" controlId="formBasicType">
	        							  <Form.Label>Type:</Form.Label>
	        							  <Form.Control 
	        							  type="string" 
	        							  placeholder="Type either wok, topping, or sauce in lowercase" 
	        							  value={typeB}
	        							  onChange = {event => setTypeB(event.target.value)}
	        							  required
	        							  />
	        							</Form.Group>
	        						</Col>
	        					</Row>
	        				      
	        					<Row>
	        						<Form.Group className="mb-3" controlId="formBasicPrice">
	        						  <Form.Label>Price:</Form.Label>
	        						  <Form.Control 
	        						  type="number" 
	        						  placeholder="Set product price" 
	        						  value={priceB}
	        						  onChange = {event => setPriceB(event.target.value)}
	        						  required
	        						  />
	        						</Form.Group>
	        					      <Form.Group className="mb-3" controlId="formBasicDescription">
	        					        <Form.Label>Description:</Form.Label>
	        					        <Form.Control 
	        					        as="textarea" 
	        					        rows={6} 
	        					        placeholder="Describe the product" 
	        					        value={descriptionB}
	        					        onChange = {event => setDescriptionB(event.target.value)}
	        					        required
	        					        />
	        					      </Form.Group>
	        			      	</Row>
	        			      	<Row>
	        			      		<Form.Group className="mb-3" controlId="formBasicImage">
	        			      		  <Form.Label>Image URL:</Form.Label>
	        			      		  <Form.Control 
	        			      		  type="string"
	        			      		  placeholder="Paste image url here" 
	        			      		  value={imageB}
	        			      		  onChange = {event => setImageB(event.target.value)}
	        			      		  />
	        			      		</Form.Group>
	        			      	</Row>
	        			      		<Button className="ml-auto" variant="primary" type="submit"onClick={(event)=>setShow(false)}>
	        			      		  	Save Changes
	        			      		</Button>
	        			    </Form>
	        		    </Col>
	        	    </Row>
		        </Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={(event)=>setShow(false)}>
		            Close
		          </Button>
		        </Modal.Footer>
	    </Modal>
    </Fragment>
		)
};