import {useContext, useState, useEffect} from 'react';
import {Container, Row, Col, Card} from 'react-bootstrap';
import UserContext from '../../UserContext.js';

export default function OrderItems ({orderProp}) {

	const {_id, transactionStart, purchaseDate, ingredients, ingredients: {productId}, ingredients: {quantity}, ingredients: {subtotal}, userId, address, instructions, status, total} = orderProp;

	// orderProp.ingredients.map(items => console.log(items))
	// console.log(orderProp)

	const {user} = useContext(UserContext);

	return(
		<Container className = "my-3 bg-img">
			<Row>
				<Col>
					<Card>
					      <Card.Header>Order: {_id} | Status: {status}</Card.Header>
					      <Card.Body>
					        <Card.Title>User: {userId}</Card.Title>
					        <Card.Subtitle>Initial Transaction:</Card.Subtitle>
					        <Card.Text>
					          {transactionStart}
					        </Card.Text>
					        <Card.Subtitle>End of Transaction:</Card.Subtitle>
					        <Card.Text>
					          {purchaseDate}
					        </Card.Text>
					        <Card.Subtitle>Order:</Card.Subtitle>
					        <Card.Text>
					          Product ID: {ingredients.productId}
					        </Card.Text>
					        <Card.Text>
					          Quantity: {ingredients.quantity}
					        </Card.Text>
					        <Card.Text>
					          Subtotal: {ingredients.subtotal}
					        </Card.Text>
					        <Card.Subtitle>Delivery Address:</Card.Subtitle>
					        <Card.Text>
					          {address}
					        </Card.Text>
					        <Card.Subtitle>Special Instructions:</Card.Subtitle>
					        <Card.Text>
					          {instructions}
					        </Card.Text>
					        <Card.Subtitle>Total:</Card.Subtitle>
					        <Card.Text>
					          {total}
					        </Card.Text>
					      </Card.Body>
				    </Card>
				</Col>
			</Row>
		</Container>
		)
}