import Swal from 'sweetalert2';
import ProdItems from './ProdItems.js';

export default function ArchiveItem({_id}) {

	const archive = ({_id}) => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/archive/${_id}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result=>result.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Item updated!',
					icon: 'success'
				})
			} else {
				Swal.fire({
					title: 'Update failed.',
					text: 'See console for more info.',
					icon: 'error'
				})
			}
		})
		// .catch(err=>console.log(err))
	};

	return(
		<ProdItems archive={archive}/>
		)
}