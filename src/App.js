// React items
import './App.css';
import {useState, useEffect} from 'react';

// Router
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

// UserProvider
import {UserProvider} from './UserContext.js';

// OrderProvider
import {OrderProvider} from './OrderContext.js';

// components
import AppNavBar from './components/AppNavBar.js';

// pages
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import Menu from './pages/Menu.js';
// user-only
// import Ordering from './pages/Ordering.js';
import CartOut from './pages/CartOut.js';

// admin-only
import AdminDash from './pages/admin/AdminDash.js';
import AllOrders from './pages/admin/AllOrders.js';
import AllProds from './pages/admin/AllProds.js';
import CreateProd from './pages/admin/CreateProd.js';

function App() {
  
  // UserContext set-up
  const [user, setUser] = useState(null);

  /*useEffect(() => {
    console.log(user)
  }, [user]);*/

  const unSetUser = () => {
    localStorage.removeItem('token');
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_CAPSTONE2}/user/myProfile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {

      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser(null);
      }
    })
  }, []);


  // OrderContext set-up
  const [order, setOrder] = useState(null);

/*  useEffect(()=> {
    console.log(order)
  }, [order]);*/

  const unSetOrder = () => {
    localStorage.removeItem('order')
  };

  /*useEffect(()=>{

    fetch(`http://localhost:4000/order/myOrders`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(ongoingOrder => {

      // If no order with pending status is found:
      if(!ongoingOrder){
        setOrder(null);
        localStorage.clear();
      // ensure we're starting with a clean order slate since user has no ongoing order
      } else {
        // If a pending order is found, 
        setOrder(ongoingOrder);
        let order = ongoingOrder;
        localStorage.setItem('order')
      }
    })
  }, []);*/

  return (
      <UserProvider value = {{user, setUser, unSetUser}}>
      <OrderProvider value = {{order, setOrder, unSetOrder}}>
          <Router>
            <AppNavBar/>
            <Routes>
                <Route path = "*" element = {<NotFound/>}/>
                <Route path = "/" element = {<Home/>}/>
                <Route path = "/register" element = {<Register/>}/>
                <Route path = "/login" element = {<Login/>}/>
                <Route path = "/logout" element = {<Logout/>}/>
                <Route path = "/menu" element = {<Menu/>}/>
                <Route path = "/myOrder" element = {<CartOut/>}/>
                {/*Admin-only routes*/}
                <Route path = "/admin" element = {<AdminDash/>}/>
                <Route path = "/admin/allOrders" element = {<AllOrders/>}/>
                <Route path = "/admin/allProducts" element = {<AllProds/>}/>
                <Route path = "/admin/create" element = {<CreateProd/>}/>
            </Routes>
          </Router>
      </OrderProvider>
      </UserProvider>
  );
}

export default App;
