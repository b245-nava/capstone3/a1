import {Container, Row} from 'react-bootstrap';
import {Fragment, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../../UserContext.js';
import AllOrders from './AllOrders.js';

export default function AdminDash () {

	const {user} = useContext(UserContext);

	return(

		user && user.isAdmin 
		?
		<Fragment>
		<Container>
			<Row className = "mt-4">
				<h1 className = "text-center">Welcome, Admin!</h1>
			</Row>
			<Row className = "mt-4">
				<AllOrders/>
			</Row>
		</Container>
		</Fragment>
		:
		<Navigate to = "*"/>
		)
}