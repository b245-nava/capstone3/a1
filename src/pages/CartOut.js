import {Fragment, useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import OrderContext from '../OrderContext.js';
import CartDetails from './CartDetails.js';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Container, Row, Col, Form, Button, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CartOut(){

	const {user} = useContext(UserContext);
	const {order, setOrder} = useContext(OrderContext);

	const [items, setItems] = useState([]);

	const [address, setAddress] = useState("");
	const [instructions, setInstructions] = useState("");

	useEffect(() => {

		fetch(`${process.env.REACT_APP_CAPSTONE2}/order/myOrders`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(cart => {
			// console.log(cart);
			setItems(cart.map(item => {
				return(
					<CartDetails key = {item._id} orderProp = {item}/>
					);
				})
			)
		})
		// .catch(err => console.log(err));
	}, []);

	const navigate = useNavigate();



	return(

		
		<Fragment>
			{
				order?
				<Container>
					<Row>
						<Table>
							<thead>
								<tr>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>Subtotal</th>
								</tr>
							</thead>
							{items}
						</Table>
					</Row>
				</Container>				
				:
				<Container className = "text-center mt-5">
					<img className = "my-2" src = 'https://i.ibb.co/bX34Z2T/emptycart.jpg' alt="Empty cart image by Paul Seling." id = "emptycart"/>
					<h3 className = "mt-2">Your cart is empty!</h3>
					<h5 className = "text-muted">Fill it up <Link to = "/menu">here.</Link></h5>
				</Container>				
			}
		</Fragment>
		)
}